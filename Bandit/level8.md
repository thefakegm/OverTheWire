## Level8 -> Level9

**Level Goal**

The password for the next level is stored in the file data.txt and is the only line of text that occurs only once

**Answer**

`sort data.txt | uniq -u`

**Password**

`UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR`