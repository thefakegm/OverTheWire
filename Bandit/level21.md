## Level21 -> Level22

**Level Goal**
A program is running automatically at regular intervals from cron, the time-based job scheduler. Look in /etc/cron.d/ for the configuration and see what command is being executed.


**Answer**
In the crontab for bandit22 it is cat-ing the password from one file to another. The file is in `/etc/bandit_pass/bandit22`


**Password**
`Yk7owGAcWjwMVRwrTesJEwB7WVOiILLI`
