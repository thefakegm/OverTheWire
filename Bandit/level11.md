## Level11 -> Level12

**Level Goal**

The password for the next level is stored in the file data.txt, where all lowercase (a-z) and uppercase (A-Z) letters have been rotated by 13 positions

**Answer**

`cat data.txt | tr '[A-Za-z]' '[N-ZA-Mn-za-m]'`

**Password**

`5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu`