## Level6 -> Level7

**Level Goal**

The password for the next level is stored somewhere on the server and has all of the following properties: - owned by user bandit7 - owned by group bandit6 - 33 bytes in size

**Answer**

`cd /`

`find . -size 33c -exec ls -l {} \; | grep "bandit7 bandit6"`

`cat ./var/lib/dpkg/info/bandit7.password`

**Password**

`HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs`