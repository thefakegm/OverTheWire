## Level1 -> Level2

**Level Goal**

The password for the next level is stored in a file called - located in the home directory

**Answer**

`cat ./-`

**Password**

`CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9`