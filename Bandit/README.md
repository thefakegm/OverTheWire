# login
host: bandit.labs.overthewire.org

port: 2220

username: bandit + levelnumber

password: ?

# Levels

* [level0](level0.md)
* [level1](level1.md)
* [level2](level2.md)
* [level3](level3.md)
* [level4](level4.md)
* [level5](level5.md)
* [level6](level6.md)
* [level7](level7.md)
* [level8](level8.md)
* [level9](level9.md)
* [level10](level10.md)
* [level11](level11.md)