## Level9 - Level10

**Level Goal**

The password for the next level is stored in the file data.txt in one of the few human-readable strings, beginning with several ‘=’ characters.

**Answer**

`strings data.txt | grep ===`

`look for the password`

**Password**

`truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk`